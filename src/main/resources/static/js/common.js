let isLogin = false;

$(function () {
    checkLogin();
    $(".sign-out").click(function () {
        $.get("/token/evict?token=" + getAccesstoken());
        window.location.reload();
    })
});


Date.prototype.format = function (format) {
    /*
    * 使用例子:format="yyyy-MM-dd HH:mm:ss";
    */
    var o = {
        "M+": this.getMonth() + 1, // month
        "d+": this.getDate(), // day
        "H+": this.getHours(), // hour
        "m+": this.getMinutes(), // minute
        "s+": this.getSeconds(), // second
        "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
        "S": this.getMilliseconds()// millisecond
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4
            - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? o[k]
                : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
};

function formatDate(value) {
    return new Date(value).format("yyyy-MM-dd HH:mm:ss");
}

function formatterYesNo(v) {
    if (v === true) {
        $(this).html('<i class="fa fa-check color-yes" title="是"></i>')
    } else if (v === false) {
        $(this).html('<i class="fa fa-close color-no" title="否"></i>')
    }
}

function checkLogin() {
    checkToken();
    if (!isLogin && !isNotLoginPage()) {
        window.location.href = "/login";
    }
}

function isNotLoginPage() {
    return window.location.href.indexOf("/login") >= 0;
}

function regTelephone(value) {
    return /^1[34578]\d{9}$/.test($.trim(value));
}

function regIdCardNo(value) {
    return /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test($.trim(value));
}

function checkToken() {
    let access_token = window.localStorage.getItem("access_token");
    $.ajax({
        url: "/oauth/check_token?token=" + access_token,
        type: "get",
        async: false,
        error: function (data) {
            isLogin = false;
        },
        success: function (data) {
            isLogin = true;
        }
    })
}

function storeToken(tokenObj) {
    if (tokenObj) {
        isLogin = true;
    }
    window.localStorage.setItem("access_token", tokenObj.access_token);
    window.localStorage.setItem("token_type", tokenObj.token_type);
}

function getToken() {
    return window.localStorage.getItem("token_type") + " " + window.localStorage.getItem("access_token");
}

function getAccesstoken() {
    return window.localStorage.getItem("access_token");
}

function make_base_auth(user, password) {
    let tok = user + ':' + password;
    let hash = btoa(tok);
    return "Basic " + hash;
}

//将表单转化为JSON对象
function formToJson(form) {
    let result = {};
    //获取表单的数组对象
    let fieldArray = form.serializeArray();
    //将表单转化为JSON对象
    for (let i = 0; i < fieldArray.length; i++) {
        let field = fieldArray[i];
        if (field.name in result) {
            result[field.name] += ',' + field.value;
        } else {
            result[field.name] = field.value;
        }
    }
    return result;
}


function buildEditLayerContent(json, columns) {
    let html = '<div class="edit-content">';
    $.each(json, function (key, index) {
        let title = getTitleFromTable(columns, key);
        html += '<div class="form-group"> <label >' + title + '：</label><input type="text" class="form-control" name="' + key + '" placeholder="' + title + '"></div>'
    });
    html += '</div>';
    return html;
}

function getTitleFromTable(columns, key) {
    for (let index in columns) {
        if (key === columns[index].field) {
            return columns[index].title;
        }
    }
}

function houseOpr(value, row, index) {
    return '<a href="#" data-id="' + value + '" title="添加住户" class="add-house-user text-success"><i class="fa fa-user-o"></i></a> &nbsp;&nbsp;' +
        '<a href="#" data-id="' + value + '" title="添加车辆" class="add-house-car text-warning"><i class="fa fa-car"></i></a> &nbsp;&nbsp;' +
        '<a href="#" data-id="' + value + '" title="物业缴费" class="add-house-fee text-dark"><i class="fa fa-cny"></i></a>&nbsp;&nbsp; ' +
        delRowText(value);
}


function delRowText(value, row, index) {
    return '<a href="#" data-id="' + value + '" title="删除该条记录" class="del-row text-danger"><i class="fa fa-trash"></i></a>';
}

function deleteRow(url) {
    $.ajax({
        url: url + "?access_token=" + getAccesstoken(),
        type: "delete",
        success: function (data) {
            window.location.reload();
        }
    })
}

function editRowValue(row, baseUrl, $table) {
    $.ajax({
        type: "put",
        url: baseUrl + "/edit?access_token=" + getAccesstoken(),
        data: JSON.stringify(row),
        contentType: "application/json",
        dataType: 'JSON',
        success: function (data) {
            layer.msg("修改成功");
            $table.bootstrapTable("refresh");
        }
    });
}