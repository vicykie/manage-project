package com.vicykie.admin.dao;

import com.vicykie.admin.entity.HouseInfo;

public interface HouseDAO extends BaseDAO<HouseInfo,Integer> {
}
