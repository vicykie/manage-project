package com.vicykie.admin.dao;

import com.vicykie.admin.entity.CarInfo;
import com.vicykie.admin.entity.HouseInfo;
import com.vicykie.admin.entity.UserInfo;

import java.util.List;

public interface CarInfoDAO extends BaseDAO<CarInfo,Integer>{

    long countAllByHouseInfo(HouseInfo houseInfo);

    List<CarInfo> findAllByHouseInfo(HouseInfo houseInfo);
}
