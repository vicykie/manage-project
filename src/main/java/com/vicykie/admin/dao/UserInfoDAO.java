package com.vicykie.admin.dao;

import com.vicykie.admin.entity.HouseInfo;
import com.vicykie.admin.entity.UserInfo;

import java.util.List;

public interface UserInfoDAO extends BaseDAO<UserInfo, Integer> {

    long countAllByHouseInfo(HouseInfo houseInfo);

    List<UserInfo> findAllByHouseInfo(HouseInfo houseInfo);
}
