package com.vicykie.admin.dao;

import com.vicykie.admin.entity.CardInfo;

public interface CardInfoDAO extends BaseDAO<CardInfo,Integer> {
}
