package com.vicykie.admin.dao;

import com.vicykie.admin.entity.PropertyFee;

public interface PropertyFeeDAO extends BaseDAO<PropertyFee, Integer> {
}
