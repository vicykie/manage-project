package com.vicykie.admin.dao;

import com.vicykie.admin.entity.LoginUser;

public interface LoginUserDAO extends BaseDAO<LoginUser, Integer> {
    LoginUser findByUsername(String username);
}
