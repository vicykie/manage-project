package com.vicykie.admin.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Setter
@Getter
@ConfigurationProperties(prefix = "spring.management.server")
@Configuration
public class ServerProperties {

    private String[] permitAllUrls;

}
