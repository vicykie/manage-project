package com.vicykie.admin.config;

import com.vicykie.admin.properties.ServerProperties;
import com.vicykie.admin.serivice.LoginUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
@Configuration
public class Oauth2ServerConfiguration {
    @Configuration
    @EnableResourceServer
    @EnableConfigurationProperties(ServerProperties.class)
    protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
        @Autowired
        ServerProperties serverProperties;
        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.resourceId("management").stateless(true);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            // @formatter:off
            http
                    // Since we want the protected resources to be accessible in the UI as well we need
                    // session creation to be allowed (it's disabled by default in 2.0.6)
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                    .and()
                    .authorizeRequests().antMatchers(serverProperties.getPermitAllUrls()).permitAll()
                    .and()
                    .authorizeRequests().anyRequest().authenticated()

                   ;//配置order访问控制，必须认证过后才可以访问
            // @formatter:on
        }
    }


    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {


        @Autowired
        private RedisConnectionFactory redisConnectionFactory;
        @Autowired
        private AuthenticationManager authenticationManager;

        @Autowired
        private LoginUserDetailService userDetailService;
        @Override
        public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
            //允许表单认证
            security.allowFormAuthenticationForClients();
            security.tokenKeyAccess("permitAll()").checkTokenAccess("permitAll()");
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

            clients.inMemory()
                    .withClient("app_client")
//        {bcrypt}$2a$10$nKezHHCe5DPb3p1PqBF70OwIg5qi9wEuxXq/BCF2qYyIRgW8GM83C
//                .secret(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("app_secret"))
                    .secret("$2a$10$nKezHHCe5DPb3p1PqBF70OwIg5qi9wEuxXq/BCF2qYyIRgW8GM83C")
                    .accessTokenValiditySeconds(60 * 60 * 24 * 7)
                    .refreshTokenValiditySeconds(60 * 60 * 24 * 7)
                    .resourceIds("management")
                    .authorities("app_client")
                    .scopes("app").authorizedGrantTypes("authorization_code", "refresh_token", "password")
                    .and()
                    .withClient("admin_client")
                    .secret(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("admin_client"))
                    .accessTokenValiditySeconds(60 * 60 * 24 * 7)
                    .refreshTokenValiditySeconds(60 * 60 * 24 * 7)
                    .resourceIds("management")
                    .authorities("admin_client")
                    .scopes("app").authorizedGrantTypes("authorization_code", "refresh_token", "password")
            ;
        }

        @Bean
        public TokenStore tokenStore() {
            return new RedisTokenStore(redisConnectionFactory);
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore());
            endpoints.userDetailsService(userDetailService);
        }

    }

}
