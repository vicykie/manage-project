package com.vicykie.admin.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
public class MvcConfig extends WebMvcConfigurationSupport  {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/user").setViewName("user-list");
        registry.addViewController("/house").setViewName("house-list");
        registry.addViewController("/car").setViewName("car-list");
        registry.addViewController("/card").setViewName("card-list");
        registry.addViewController("/fee").setViewName("fee-list");

//        registry.addViewController("/").setViewName("index");
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");

        super.addResourceHandlers(registry);
    }

}
