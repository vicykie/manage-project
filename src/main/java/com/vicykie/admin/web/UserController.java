package com.vicykie.admin.web;

import com.vicykie.admin.dao.UserInfoDAO;
import com.vicykie.admin.entity.UserInfo;
import com.vicykie.admin.util.PageUtil;
import com.vicykie.admin.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/user")
@RestController
@PreAuthorize("hasRole('role_user')")
public class UserController {
    @Autowired
    private UserInfoDAO userInfoDAO;

    @Autowired
    private HttpServletRequest request;

    @GetMapping("/list")
    public Map<String, Object> getList() {
        Page all = userInfoDAO.findAll(null, RequestUtil.generateRequest(request));
        return PageUtil.page2Map(all);
    }

    @PutMapping("edit")
    public ResponseVO editUser(@RequestBody UserInfo userInfo) {
        userInfoDAO.save(userInfo);
        return ResponseVO.ok();
    }

    @PostMapping("/add")
    public UserInfo addUser(@RequestBody UserInfo userInfo) {
        return userInfoDAO.save(userInfo);
    }

    @DeleteMapping("/{id}")
    public ResponseVO delete(@PathVariable("id") int id) {
        userInfoDAO.deleteById(id);
        return ResponseVO.ok();
    }
}
