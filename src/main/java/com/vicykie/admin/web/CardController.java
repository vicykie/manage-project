package com.vicykie.admin.web;

import com.vicykie.admin.dao.CarInfoDAO;
import com.vicykie.admin.dao.CardInfoDAO;
import com.vicykie.admin.entity.CarInfo;
import com.vicykie.admin.entity.CardInfo;
import com.vicykie.admin.entity.HouseInfo;
import com.vicykie.admin.util.PageUtil;
import com.vicykie.admin.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/card")
@Controller
@PreAuthorize("hasRole('role_user')")
public class CardController {
    @Autowired
    private CardInfoDAO cardInfoDAO;
    @Autowired
    private HttpServletRequest request;

    @GetMapping("/list")
    @ResponseBody
    public Map<String, Object> getList() {
        Page all = cardInfoDAO.findAll(null, RequestUtil.generateRequest(request));
        return PageUtil.page2Map(all);
    }

    @PostMapping("/add")
    @ResponseBody
    public CardInfo addHouse(@RequestBody CardInfo cardInfo) {
        return cardInfoDAO.save(cardInfo);
    }

    @PutMapping("/edit")
    @ResponseBody
    public ResponseVO editUser(@RequestBody CardInfo cardInfo) {
        cardInfoDAO.save(cardInfo);
        return ResponseVO.ok();
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseVO delete(@PathVariable("id") int id) {
        cardInfoDAO.deleteById(id);
        return ResponseVO.ok();
    }
}
