package com.vicykie.admin.web;

import com.vicykie.admin.dao.CarInfoDAO;
import com.vicykie.admin.dao.HouseDAO;
import com.vicykie.admin.entity.CarInfo;
import com.vicykie.admin.entity.CardInfo;
import com.vicykie.admin.entity.HouseInfo;
import com.vicykie.admin.util.PageUtil;
import com.vicykie.admin.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/car")
@Controller
@PreAuthorize("hasRole('role_user')")
public class CarController {
    @Autowired
    private CarInfoDAO carInfoDAO;
    @Autowired
    private HttpServletRequest request;

    @GetMapping("/list")
    @ResponseBody
    public Map<String, Object> getList() {
        Page paged = carInfoDAO.findAll(null, RequestUtil.generateRequest(request));
        return PageUtil.page2Map(paged);
    }

    @PostMapping("/add")
    @ResponseBody
    public CarInfo addHouse(@RequestBody CarInfo carInfo) {
        return carInfoDAO.save(carInfo);
    }

    @PutMapping("/edit")
    @ResponseBody
    public ResponseVO editUser(@RequestBody CarInfo carInfo) {
        carInfoDAO.save(carInfo);
        return ResponseVO.ok();
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseVO delete(@PathVariable("id") int id) {
        carInfoDAO.deleteById(id);
        return ResponseVO.ok();
    }
}
