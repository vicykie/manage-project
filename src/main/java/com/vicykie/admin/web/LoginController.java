package com.vicykie.admin.web;

import com.vicykie.admin.dao.CarInfoDAO;
import com.vicykie.admin.dao.CardInfoDAO;
import com.vicykie.admin.dao.HouseDAO;
import com.vicykie.admin.dao.UserInfoDAO;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {

    @Autowired
    private UserInfoDAO userInfoDAO;
    @Autowired
    private CarInfoDAO carInfoDAO;
    @Autowired
    private CardInfoDAO cardInfoDAO;
    @Autowired
    private HouseDAO houseDAO;

    @Autowired
    private ConsumerTokenServices consumerTokenServices;

    @GetMapping("/login")
    public String toLoginPage(Model model) {
        return "login";
    }

    /**
     * 退出登录
     *
     * @return
     */

    @GetMapping("/token/evict")
    @ResponseBody
    public ResponseVO evictToken(@RequestParam("token") String token) {
        consumerTokenServices.revokeToken(token);
        return ResponseVO.ok();
    }

    @GetMapping("/error")
    @ResponseBody
    public String errorPage() {
        return "error";
    }

    @GetMapping("")
    public String index() {
        return "redirect:/index";
    }

    @GetMapping("/index")
    public String indexPage(Model model) {
        model.addAttribute("userCount", userInfoDAO.count());
        model.addAttribute("carCount", carInfoDAO.count());
        model.addAttribute("carCount", carInfoDAO.count());
        model.addAttribute("cardCount", cardInfoDAO.count());
        model.addAttribute("houseCount", houseDAO.count());
        return "index";
    }
}
