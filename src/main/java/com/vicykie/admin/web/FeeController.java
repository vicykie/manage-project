package com.vicykie.admin.web;

import com.vicykie.admin.dao.CarInfoDAO;
import com.vicykie.admin.dao.PropertyFeeDAO;
import com.vicykie.admin.entity.CarInfo;
import com.vicykie.admin.entity.PropertyFee;
import com.vicykie.admin.util.PageUtil;
import com.vicykie.admin.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/fee")
public class FeeController {
    @Autowired
    private PropertyFeeDAO propertyFeeDAO;
    @Autowired
    private HttpServletRequest request;

    @GetMapping("/list")
    public Map<String, Object> getList() {
        Page paged = propertyFeeDAO.findAll(null, RequestUtil.generateRequest(request));
        return PageUtil.page2Map(paged);
    }

    @PostMapping("/add")
    public PropertyFee addFee(@RequestBody PropertyFee propertyFee) {
        return propertyFeeDAO.save(propertyFee);
    }

    @PutMapping("/edit")
    public ResponseVO editUser(@RequestBody PropertyFee propertyFee) {
        propertyFeeDAO.save(propertyFee);
        return ResponseVO.ok();
    }

    @DeleteMapping("/{id}")
    public ResponseVO delete(@PathVariable("id") int id) {
        propertyFeeDAO.deleteById(id);
        return ResponseVO.ok();
    }
}
