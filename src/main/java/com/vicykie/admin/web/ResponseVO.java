package com.vicykie.admin.web;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseVO implements Serializable {

    private Object data;
    private boolean success;

    public ResponseVO(Object data, boolean success) {
        this.data = data;
        this.success = success;
    }


    public static ResponseVO ok() {
        return ok(null);
    }


    public static ResponseVO ok(Object data) {
        return new ResponseVO(data, true);
    }
}
