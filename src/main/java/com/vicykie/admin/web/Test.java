package com.vicykie.admin.web;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Test {

    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String raw1 = "app_secret";
        String raw = "123456";
        System.out.println(encoder.encode(raw1));
        System.out.println(encoder.encode(raw));
        System.out.println(encoder.matches("app_secret", "$2a$10$nKezHHCe5DPb3p1PqBF70OwIg5qi9wEuxXq/BCF2qYyIRgW8GM83C"));
        System.out.println(encoder.matches("123456", "$2a$10$yTAKJMdrrl560BnrI6KY5uaRtnmIF5GBJjczi.koJTpBb0TAjzfIW"));
    }
}
