package com.vicykie.admin.web;

import com.vicykie.admin.dao.CarInfoDAO;
import com.vicykie.admin.dao.HouseDAO;
import com.vicykie.admin.dao.PropertyFeeDAO;
import com.vicykie.admin.dao.UserInfoDAO;
import com.vicykie.admin.entity.CarInfo;
import com.vicykie.admin.entity.HouseInfo;
import com.vicykie.admin.entity.PropertyFee;
import com.vicykie.admin.entity.UserInfo;
import com.vicykie.admin.util.PageUtil;
import com.vicykie.admin.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.SimpleFormatter;

@RequestMapping("/house")
@RestController
@PreAuthorize("hasRole('role_user')")
public class HouseController {
    @Autowired
    private HouseDAO houseDAO;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private UserInfoDAO userInfoDAO;
    @Autowired
    private PropertyFeeDAO propertyFeeDAO;

    @Autowired
    private CarInfoDAO carInfoDAO;

    @GetMapping("/list")
    public Map<String, Object> getList() {
        Page all = houseDAO.findAll(null, RequestUtil.generateRequest(request));
        return PageUtil.page2Map(all);
    }

    @PostMapping("/user")
    public ResponseVO addHouseUser(@RequestBody UserInfo userInfo) {
        userInfoDAO.save(userInfo);
        Optional<HouseInfo> houseInfo = houseDAO.findById(userInfo.getHouseInfo().getId());
        houseInfo.ifPresent(h -> {
            long count = userInfoDAO.countAllByHouseInfo(h);
            h.setUserCount(count);
            houseDAO.save(h);
        });
        return ResponseVO.ok();
    }

    @PostMapping("/car")
    public ResponseVO addHouseUser(@RequestBody CarInfo carInfo) {
        carInfoDAO.save(carInfo);
        Optional<HouseInfo> houseInfo = houseDAO.findById(carInfo.getHouseInfo().getId());
        houseInfo.ifPresent(h -> {
            long count = carInfoDAO.countAllByHouseInfo(h);
            h.setCarCount(count);
            houseDAO.save(h);
        });
        return ResponseVO.ok();
    }

    @PostMapping("/fee")
    public ResponseVO addHouseFee(@RequestBody PropertyFee propertyFee, Principal principal) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        propertyFee.setPaidYear(format.format(new Date()));
        propertyFeeDAO.save(propertyFee);
        return ResponseVO.ok();
    }


    @PostMapping("/add")
    public HouseInfo addHouse(@RequestBody HouseInfo houseInfo) {
        return houseDAO.save(houseInfo);
    }

    @PutMapping("/edit")
    public ResponseVO editUser(@RequestBody HouseInfo houseInfo) {
        houseDAO.save(houseInfo);
        return ResponseVO.ok();
    }

    @DeleteMapping("/{id}")
    public ResponseVO delete(@PathVariable("id") int id) {
        List<UserInfo> userInfos = userInfoDAO.findAllByHouseInfo(new HouseInfo(id));
        List<CarInfo> carInfos = carInfoDAO.findAllByHouseInfo(new HouseInfo(id));
        userInfoDAO.deleteAll(userInfos);
        carInfoDAO.deleteAll(carInfos);
        houseDAO.deleteById(id);
        return ResponseVO.ok();
    }
}
