package com.vicykie.admin.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

    public static Pageable generateRequest(HttpServletRequest request) {
        String offset = request.getParameter("offset");
        String limit = request.getParameter("limit");
        String sort = request.getParameter("sort");
        String order = request.getParameter("order");
        if (StringUtils.isNotEmpty(offset) && StringUtils.isNotEmpty(limit)) {
            return PageRequest.of(Integer.valueOf(offset) / Integer.valueOf(limit), Integer.valueOf(limit), Sort.Direction.valueOf(order.toUpperCase()), sort);
        }
        return PageRequest.of(0, 15, Sort.Direction.ASC, "id");
    }
}
