package com.vicykie.admin.util;

import org.springframework.data.domain.Page;

import java.util.HashMap;
import java.util.Map;

public class PageUtil {

    public static Map<String, Object> page2Map(Page page) {
        Map<String, Object> map = new HashMap<>();
        map.put("rows", page.getContent());
        map.put("total", page.getTotalElements());
        return map;
    }
}
