package com.vicykie.admin.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 登录用户
 */
@Data
@Entity
@Table(name = "t_login_user")
public class LoginUser implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String telephone;
    private String password;
    private String email;
    private boolean enabled;
    private Date lastLoginDate;
    private Date createDate = new Date();
}
