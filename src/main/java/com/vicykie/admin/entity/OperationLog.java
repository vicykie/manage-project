package com.vicykie.admin.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 操作日志
 */
@Data
@Table(name = "t_opr_log")
@Entity
public class OperationLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "login_user_id")
    private LoginUser loginUser;

    @Enumerated(EnumType.STRING)
    private OperationType operationType;

    private Boolean operationSuccess;

    private String parameters;
    private String className;
    private String methodName;
    private Date createDate = new Date();

    private String ip;

    private Date operationDate;

    public enum OperationType {
        LOGIN,
        ADD_USER,
        UPDATE_USER,
        DELETE_USER,
        ADD_HOUSE,
        UPDATE_HOUSE,
        ADD_CARD,
    }
}
