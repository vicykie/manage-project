package com.vicykie.admin.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_property_fee")
@Data
public class PropertyFee implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date createDate = new Date();

    private Date updateDate;
    @ManyToOne
    @JoinColumn(name = "house_id")
    private HouseInfo houseInfo;
    /**
     * 缴费年度
     */
    private String paidYear;

    /**
     * 物业费单价
     */
    private String propertyFee;

    /**
     * 物业费金额
     */
    private String propertyTotal;

    @ManyToOne
    @JoinColumn(name = "opr_user_id")
    private LoginUser oprUser;
}
