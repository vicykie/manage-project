package com.vicykie.admin.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "t_car_info")
@DynamicUpdate
public class CarInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * 车牌
     */
    private String carPlateNo;

    /**
     * 车辆品牌
     */
    private String carBrand;
    /**
     * 型号
     */
    private String carModel;

    /**
     * 颜色
     */
    private String carColor;

    /**
     * 长期使用人
     */
    private String carUser;

    /**
     * 是否有车位
     */
    private Boolean hasCarPosition;
    /**
     * 车辆状况
     */
    private String carSituation;

    @ManyToOne
    @JoinColumn(name = "house_id")
    private HouseInfo houseInfo;
    private Date createDate = new Date();
    private Date updateDate;

}
