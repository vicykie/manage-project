package com.vicykie.admin.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 户主信息
 */
@Data
@Entity
@Table(name = "t_user_info")
@DynamicUpdate
public class UserInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * 名字
     */
    private String name;

    /**
     * 性别
     */
    @Enumerated(EnumType.STRING)
    private Gender gender;
    /**
     * 与户主关系
     */
    private String relationship;
    /**
     * 身份证号码
     */
    private String idCardNo;
    /**
     * 电话号码
     */
    private String telephone;
    /**
     * 职业
     */
    private String occupation;
    /**
     * 惠誉生活卡
     */
    /**
     * 是否加微信
     */
    private Boolean beWechatWith;
    /**
     * 是否长期住小区
     */
    private Boolean livedLong;
    /**
     * 是否关注微信公众号
     */
    private Boolean hasAttentionWechat;
    /**
     * 是否学生
     */
    private Boolean isStudent;
    /**
     * 就读学校
     */
    private String school;
    /**
     * 就读年级
     */
    private String grade;
    /**
     * 是否60岁以上
     */
    private Boolean olderThanSixty;

    /**
     * 是否户主
     */
    private boolean householder;


    private Date createDate = new Date();
    private Date updateDate;


    @ManyToOne
    @JoinColumn(name = "house_id")
    private HouseInfo houseInfo;

    public enum Gender {
        MALE, FEMALE
    }
}
