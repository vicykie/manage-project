package com.vicykie.admin.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "t_house_info")
@DynamicUpdate
public class HouseInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public HouseInfo(int id) {
        this.id = id;
    }

    /**
     * 小区名称
     */
    private String communityName;
    /**
     * 楼号
     */
    private String buildingNo;

    /**
     * 单元号
     */
    private String unitNo;

    /**
     * 户号
     */
    private String houseNo;

    /**
     * 是否装修
     */
    private Boolean decorated;
    /**
     * 是否入住
     */
    private Boolean checkedIn;
    /**
     * 房屋面积
     */
    private String houseArea;
    /**
     * 车库/位面积
     */
    private String carArea;
    /**
     * 车辆数量
     */
    private long carCount;
    /**
     * 住户数量
     */
    private long userCount;

    private Date createDate = new Date();

    private Date updateDate;

/**
 * 缴费情况（年度计15/16/17）
 * 截止11/30
 */

/**
 * 出租情况
 */
/**
 * 车辆情况
 */
/**
 * 车辆蓝牙发放
 */


}
