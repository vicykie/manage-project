package com.vicykie.admin.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 会员卡
 */
@Data
@Entity
@Table(name = "t_card_info")
@DynamicUpdate
public class CardInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * 卡号
     */
    private String cardNo;
    /**
     * 是否启用
     */
    private boolean enabled;
    /**
     * 标签
     */
    private String tags;
    private Date createDate = new Date();
    private Date updateDate;

}
